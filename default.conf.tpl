server {
    listen ${LISTEN_PORT};

    location /static {
        aliase /vol/static;
    }

    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uswgi_params;
        clent_max_body_size     10M;

    }
}
